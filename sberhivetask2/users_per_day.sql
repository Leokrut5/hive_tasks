SET hive.query.name="users_per_day";

USE tkalichle;

SELECT
    `date`,
    count(1) as cnt
FROM Logs
GROUP BY `date`
ORDER BY cnt DESC;
