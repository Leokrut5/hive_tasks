#!/usr/bin/env python

from pyspark.sql import SparkSession
from pyspark import SparkContext
from pyspark import SparkConf
import pyspark.sql.functions as f
from pyspark.sql.types import *
import re

schema = StructType(fields=[
    StructField("article_ID", IntegerType()),
    StructField("article_text", StringType())
])
spark = SparkSession.builder.appName('Spark DF task1_1').master('yarn').getOrCreate()
sc=spark.sparkContext
rdd = sc.textFile("/data/wiki/en_articles_part").map(lambda x: x.split("\t"))
rdd2 = rdd.map(lambda x: (int(x[0]), re.sub("/[^a-z ]/iu", "", x[1]).lower()))
df = spark.createDataFrame(rdd2, schema=schema)
res = df.select("article_ID", f.regexp_extract('article_text', r'(narodnaya\s+\w+)+', 1)\
        .alias('pair')).select(f.regexp_replace('pair',r' ', '_').alias('pair'))\
        .filter("pair <> '' ").groupby('pair').count().sort(['pair'], ascending=True).cache().take(10)
for i in res:
    #print(i["pair"], i["count"])
    print("{} {}".format(i["pair"], str(i["count"])))
