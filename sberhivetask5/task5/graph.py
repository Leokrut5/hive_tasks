#! /usr/bin/env python3

from matplotlib import pyplot as plt

x = []
y = []
with open('points.txt') as f:
    for line in f.readlines():
        fields = line.strip().split('\t')
        y.append(fields[1])
        x.append(fields[0])

plt.plot(x,y)

plt.title("Graph")
plt.ylabel('Users_in_Moscow')
plt.xlabel('Percentage')
plt.savefig('graph.png')
