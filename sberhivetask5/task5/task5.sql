USE leokrut5;

SET hive.query.name="task5_test";
SET hive.auto.convert.join = false;

INSERT INTO TABLE task5
select
    1,
    SUM(men) AS c_men
FROM (
    SELECT
        IF(gender='male', count(1), 0) AS men,
        IF(gender='female', count(1), 0) AS women,
        t3.region
    FROM Logs TABLESAMPLE(1 PERCENT)
    INNER JOIN(
        SELECT
            gender,
            ip
        FROM Users
    ) AS t2
    ON Logs.ip = t2.ip
    INNER JOIN (
    SELECT
        region,
        ip
    FROM IPRegions
    WHERE region = 'Moscow'
    ) as t3
    ON t3.ip = Logs.ip
    GROUP BY t3.region, t2.gender
) as t4
GROUP BY t4.region;

SELECT * FROM task5 ORDER BY percantage;
