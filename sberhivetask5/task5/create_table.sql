USE leokrut5;

DROP TABLE IF EXISTS task5;

CREATE EXTERNAL TABLE task5 (
    percantage INT,
    people INT
)
STORED AS TEXTFILE;

