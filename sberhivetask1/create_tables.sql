USE tkalichle;

SET hive.query.name="partitioning_logs";
SET hive.exec.dynamic.partition.mode=nonstrict;
SET hive.exec.max.dynamic.partitions=116;
SET hive.exec.max.dynamic.partitions.pernode=116;

DROP TABLE IF EXISTS Logs_not_partition;
DROP TABLE IF EXISTS Users;
DROP TABLE IF EXISTS IPRegions;
DROP TABLE IF EXISTS Subnets;
DROP TABLE IF EXISTS Logs;


CREATE EXTERNAL TABLE Logs_not_partition (
    ip STRING,
    `date` INT,
    http_request STRING,
    page_volume INT,
    http_status INT,
    browser STRING
) ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.RegexSerDe'
WITH SERDEPROPERTIES (
    "input.regex" = '^(\\S+)\\s+(\\d{8})\\d+\\s+(\\S+)\\s+(\\d+)\\s+(\\d+)\\s+(.*)$'
)

STORED AS TEXTFILE
LOCATION '/data/user_logs/user_logs_M';

CREATE EXTERNAL TABLE Users(
    ip STRING,
    browser STRING,
    gender STRING,
    age INT
)
ROW FORMAT DELIMITED FIELDS TERMINATED BY  '\t'
STORED AS TEXTFILE
LOCATION '/data/user_logs/user_data_M';

CREATE EXTERNAL TABLE IPRegions(
    ip STRING,
    region STRING
)
ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION '/data/user_logs/ip_data_M/';


CREATE EXTERNAL TABLE Subnets (
    ip STRING,
    mask STRING
)
ROW FORMAT DELIMITED FIELDS TERMINATED BY  '\t'
STORED AS TEXTFILE
LOCATION '/data/subnets/variant1';


CREATE EXTERNAL TABLE Logs (
    ip STRING,
    http_request STRING,
    page_volume INT,
    http_status INT,
    browser STRING
)
PARTITIONED BY (`date` INT)
STORED AS TEXTFILE;

INSERT OVERWRITE TABLE Logs PARTITION (`date`)
SELECT
     ip,
    http_request,
    page_volume,
    http_status,
    browser,
    `date`
FROM Logs_not_partition;


SELECT * FROM Logs LIMIT 10;
SELECT * FROM Users LIMIT 10;
SELECT * FROM IPRegions LIMIT 10;
SELECT * FROM Subnets LIMIT 10;
