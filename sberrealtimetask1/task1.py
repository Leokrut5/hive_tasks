#!/usr/bin/env python

from pyspark.sql import SparkSession
from pyspark import SparkContext
from pyspark import SparkConf
import pyspark.sql.functions as f
from pyspark.sql.types import *
import re


schema = StructType(fields=[
    StructField("article_ID", IntegerType()),
    StructField("article_text", StringType())
])

spark = SparkSession.builder.appName('Spark DF task1_1').master('yarn').getOrCreate()
sc=spark.sparkContext

rdd = sc.textFile("/data/wiki/en_articles_part").map(lambda x: x.split("\t"))
rdd2 = rdd.map(lambda x: (int(x[0]), re.sub("[^a-z0-9.']", " ", x[1].lower()))).map(lambda x: (x[0], re.sub("\s+", " ", x[1])))
df = spark.createDataFrame(rdd2, schema=schema)
words_schema = StructType(fields=[
    StructField("word", StringType())
])

stop_words_df = spark.read.csv("/data/wiki/stop_words_en-xpo6.txt", schema=words_schema)

words = df.select('article_text', f.split('article_text', ' ').alias('words')).select(f.explode('words').alias('words_exploded'))\
    .groupby('words_exploded').count().orderBy("count", ascending=False).cache()
all_words = words.select(f.sum('count').alias('sum_words'))
words = words.join(stop_words_df, stop_words_df.word == words.words_exploded, how='leftanti')

def function1(x):
    s = ''
    array = x[1].split()
    for i in range(len(array) - 1):
        s += '_'.join([array[i], array[i+1]]) + ' '
    print(s)
    return (int(x[0]), s)

rdd3 = df.rdd.map(function1)
df_copy = spark.createDataFrame(rdd3, schema=schema)
pairs = df_copy.select('article_text', f.split('article_text', ' ').alias('pairs')).select(f.explode('pairs').alias('pairs_exploded'))\
    .groupby('pairs_exploded').count().orderBy("count", ascending=False)
all_pairs = pairs.select(f.sum('count').alias('sum_pairs'))
pairs = pairs.withColumn("first_word", f.regexp_extract('pairs_exploded', r'(\w+)_.*', 1)).withColumn("second_word", f.regexp_extract('pairs_exploded', r'.*_(\w+)', 1)).cache()

condition = [(stop_words_df.word == pairs.second_word) | (stop_words_df.word == pairs.first_word)]
final_pairs = pairs.join(stop_words_df, condition, how='leftanti').where("count > 500")
final_pairs = final_pairs.select('pairs_exploded', f.col("count").alias("count_pairs"), 'first_word', 'second_word')

final_pairs = final_pairs.join(words, words.words_exploded == final_pairs.second_word, how='inner')
final_pairs = final_pairs.select('pairs_exploded', 'count_pairs', 'first_word', 'second_word', f.col("count").alias("count_second_word"))
final_pairs = final_pairs.join(words, words.words_exploded == final_pairs.first_word, how='inner')
final_pairs = final_pairs.select('pairs_exploded', 'count_pairs', 'first_word', 'second_word', f.col("count").alias("count_first_word"), 'count_second_word')

final_pairs = final_pairs.withColumn("probability_pair", final_pairs.count_pairs / 12134477)\
                         .withColumn("probability_first", final_pairs.count_first_word / 12138551)\
                         .withColumn("probability_second", final_pairs.count_second_word / 12138551)

final_pairs = final_pairs['pairs_exploded', 'first_word', 'second_word','probability_pair', 'probability_first', 'probability_second']
final_pairs = final_pairs.withColumn("PMI", f.log(final_pairs.probability_pair / (final_pairs.probability_first * final_pairs.probability_second)))\
                         .withColumn("ln_pair", f.log(final_pairs.probability_pair))
final_pairs = final_pairs['pairs_exploded', 'first_word', 'second_word', 'PMI', 'ln_pair']

result = final_pairs.withColumn('NPMI', final_pairs.PMI / final_pairs.ln_pair * -1)\
                    .select(['pairs_exploded', 'NPMI']).filter("pairs_exploded != '' ")\
                    .orderBy('NPMI', ascending=False).take(39)
for i in result:
    print("{}".format(i["pairs_exploded"]))
