USE tkalichle;

SET hive.query.name="task3_test";
SET hive.auto.convert.join = false;

select
    t4.region,
    SUM(men) AS c_men,
    SUM(women) AS c_women
FROM (
    SELECT
        IF(gender='male', count(1), 0) AS men,
        IF(gender='female', count(1), 0) AS women,
        t3.region
    FROM Logs
    INNER JOIN(
        SELECT
            gender,
            ip
        FROM Users
    ) AS t2
    ON Logs.ip = t2.ip
    INNER JOIN (
    SELECT
        region,
        ip
    FROM IPRegions
    ) as t3
    ON t3.ip = Logs.ip
    GROUP BY t3.region, t2.gender
) as t4
GROUP BY t4.region;
